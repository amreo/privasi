# Compartiche?

Abbiamo imparato finora a ridurre il più possibile le possibilità di tracciamento, sia usando programmi di cui possiamo conoscere il funzionamento, sia bloccando elementi come i cookie quando navighiamo su internet. Quello che faremo ora è invece tamponare il problema nel caso qualcosa dovesse andare storto, perché... beh, molto onestamente, può capitare. Succede a tutti infatti di fare errori di distrazione, senza contare che alcuni siti continuano a studiare i metodi più variopinti per tracciare l'utenza da una parte all'altra della rete. E dato che non possiamo umanamente conoscere tutto quello che passa per la testa di questi siti né essere tutti ingegneri informatici per analizzare codice 24 ore su 24, l'unica soluzione rimasta è arginare. E quell'arginare si chiama compartimentazione.  

Compartimentare significa suddividere le cose in compartimenti stagni, in questo caso i nostri comportamenti. È probabile che molti di voi già lo facciano in parte senza saperlo: l'esempio più lampante è avere due mail, una professionale e l'altra per tutto il resto. Questo infatti non dà solo più ordine, ma permette di tenere separate le attività collegate a voi (magari la mail professionale contiene il vostro nome e cognome) da quelle più *casual*. Se infatti la mail non professionale dovesse venire compromessa, non dovreste rifare anche l'altra.

Quello che faremo è quindi creare ambienti separati, attraverso strumenti e piccole accortezze; cosicché se in uno di questi ambienti dovesse succedere qualcosa, non ne risentirebbe la nostra identità in toto ma solo una fetta. Ridurremo, in altre parole, il danno e la sua estensione.
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  
