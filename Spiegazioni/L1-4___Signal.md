# Conversazione sensibili pt.2: Signal incontra WhatsApp

> WhatsApp è l'app di messaggistica più usata al mondo, fungendo da vero e proprio social in certi stati. Quanto è privato? Ci sono alternative sulla piazza? E come è possibile anche solo immaginare che altre persone possano considerare altre opzioni rispetto a questo colosso di messaggistica?

## In parole semplici
Cosa rende una conversazione privata? Spesso c'è confusione tra cos'è anonimo e privato, quindi per capirlo meglio immaginiamo che ci siano due persone chiuse in una stanza a parlare.  
Se NON abbiamo visto chi sono ma sappiamo **cosa** si stanno dicendo, la conversazione è anonima. Al contrario, se NON sappiamo *cosa* stanno dicendo ma sappiamo *chi* c'è dentro la stanza, la conversazione è privata.  
Quindi, possiamo dire che una conversazione è privata quando solo gli interlocutori possono sentire/vedere cosa viene detto. Sotto questo punto di vista, sia WhatsApp che Signal sono private.

Mettiamo, ora, che prima di entrare nella stanza ci sia una telecamera per vedere chi entra, quando, come e con chi: analizzando la registrazione si possono quindi capire le abitudini, con chi si parla di più, quando si è soliti entrare ecc. facendosi un'idea della persona.  
In informatica, chiamiamo questi dati di contorno **metadati**. E proprio i metadati son quelli che WhatsApp da contratto¹ passa a Facebook (perché appartiene a Facebook²) e a terze parti, per una migliore profilazione: ultimo accesso, quanto state connessi, con chi chattate di più, numero di telefono, numeri in rubrica, connessione internet, posizione GPS ecc.. Anche se non sanno cosa venga scritto, quindi, grazie ai metadati possono capire molto di noi.  

Sarà poi capitato di leggere che le chat di WhatsApp sono protette da "crittografia end-to-end". In parole povere, è un modo per impedire a esterni, azienda inclusa, di poterle leggere. Tuttavia se non abbiamo un telefono Apple, quando i nostri messaggi vengono salvati sul cloud (ovvero un *backup*, che in questo caso viene fatto su Google Drive), i messaggi *non* sono crittografati. Questo permette a chiunque abbia l'accesso a quel determinato Google Drive di poter leggere le chat in chiaro³.  

Infine, proprio come Messenger, WhatsApp non è open source, privandoci di sapere cosa succede dietro le quinte. Se aggiungiamo come se la passa Facebook con la privacy e l'onestà, riteniamo sia un ulteriore motivo per non affidarcisi.  

"Sì, ma tutti quelli che conosco usano WhatsApp". E avete ragione: isolarsi dal mondo sarebbe stupido.  
È per questo che infatti vi consigliamo di AFFIANCARE l'app di Signal a Whatsapp. Signal ha la stessa protezione, con l'aggiunta che non colleziona metadati, è completamente open source, non manda backup a terze parti mettendo a rischio la vostra privacy, ha un'intera community di supporto alla quale potete prendere parte se masticate l'inglese⁴, supporta messaggi che si autodistruggono e, inoltre, se avete Android vi permette di poter leggere e scrivere anche SMS (come unire WhatsApp e SMS nella stessa app).

## Cosa fare
Come Telegram, trovate Signal sullo store delle app. Installatela, seguite le istruzioni, dategli il permesso per gli SMS se avete Android e.. fine!  

Veniamo invece al lato sociale: chi usa Signal a parte me?  
Quando provate a scrivere a qualcuno, se la sua iniziale nella rubrica di Signal è azzurra vuol dire che ha l'app come voi (se avete Apple vedrete SOLO chi ha l'app). In caso contrario, scrivergli equivarrà a scrivere un SMS (che non è crittato).  

Ci si potrebbe chiedere: come porto la gente su Signal (o qualsiasi altra app)?  
Ma, se ci si pensa su, non è propriamente la domanda corretta. La domanda corretta è invece *perché* dovrei portare qualcuno su Signal.  

Potremmo dirvi di parlarne con chiunque vi capiti a tiro per convincerli, ma la verità è che a forzare un argomento si appare pesanti e si ottiene l'effetto contrario. Certo, potete accennarlo, ma se non volete diventare lo stereotipo del vegano fastidioso (che poi mette in cattiva luce tutti gli altri vegani), non insistete.  

Parliamone invece con le persone che *ci stanno a cuore*. Per esempio, con i nostri genitori (più propensi ad ascoltarci di uno sconosciuto, soprattutto se non hanno dimestichezza con la tecnologia), il nostro partner o l'amico d'infanzia. Questo perché sono persone alle quali teniamo e le quali non dovrebbero avere problemi a scaricare un'app in più per noi. E, ancora più importante, sono persone con le quali abbiamo un rapporto più intimo, privato, che non dovremmo voler "contaminato" da aziende sulle quali potremmo speculare all'infinito senza aver risposte (finché non salta fuori l'ennesima inchiesta). E chissà che anche loro non abbiano altre persone importanti a cui dirlo.

Infine, ora che avete sia un'app per le conversazioni "pubbliche" e una per quelle più sensibili, sta a voi decidere quando usare una o l'altra: chiedetevi quando scrivete: "quello che sto per dire contiene dati sensibili (per esempio dati personali, foto dei vostri figli -che vi consiglieremmo a prescindere di non inviare né caricare su internet-, documenti aziendali ecc.), o è un'informazione generica?"

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Tecnicamente
[DA AGGIUNGERE]  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹ Contratto WhatsApp: https://www.whatsapp.com/legal/?eea=1&lang=it  
² Covert Adrian, [Facebook buys WhatsApp for $19 billion](https://money.cnn.com/2014/02/19/technology/social/facebook-whatsapp/index.html), CNN, 2014  
³ Osborne Charlie, [WhatsApp warns free Google Drive backups are not end-to-end encrypted](https://www.zdnet.com/article/whatsapp-warns-free-google-drive-backups-are-not-encrypted/), ZDNet, 2018  
⁴ https://community.signalusers.org/  
