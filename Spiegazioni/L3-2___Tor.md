# Personale: Tor, cipolle digitali

> Tor: quella rete dove la modalità incognito è davvero in incognito. Alla scoperta dello spazio privato

# In parole semplici

Se volete sentirvi l'hacker hollywoodiano, questo è il momento giusto: Tor nasce infatti come progetto militare per condividere informazioni in maniera sicura attraverso la rete, ed è poi stato portato avanti dalla no profit Tor Project in maniera del tutto FOSS¹. Solitamente, esso viene abbinato a cose come il *deep web* dove assoldare assassini, comprare schiavi e altre mitiche leggende che lo fan sembrare l'anticamera dell'inferno, ma ci si scorda volutamente di menzionare che è uno strumento essenziale per evadere la censura dei paesi autoritari (lo usa persino la BBC²), e che chi tira su imperi della droga come Silk Road prima o poi viene beccato (i pagamenti sono ben o male rintracciabili)³. Il compito di Tor, perciò, è quello di garantire l'anonimato di chi lo usa.  
  
Per capire come funziona, partiamo dal suo nome: Tor sta per *The Onion Router*, il router cipolla. È una cipolla perché è fatto a strati - 3 per l'esattezza - e se vi ricordate il discorso nel capitolo dell'esodo di "un braccio è buono, due sono meglio", questa è esattamente la stessa cosa applicata all'anonimato.  
  
Ognuno di questi strati è infatti un computer, che qualcuno ha messo a disposizione per chi vuole navigare tramite Tor. Quando ci connettiamo, Tor pesca 3 computer a caso tra tutti i computer disponibili (attualmente qualche migliaio), che avranno il compito di tenere al sicuro l'identità di chi c'è davvero dietro (noi). Per capirlo meglio, immaginiamo di voler inviare un pacco alla famiglia Wikipedia. Al posto di spedire il pacco direttamente al loro indirizzo - che di conseguenza svelerebbe anche dove abitiamo - lo consegniamo a un postino misterioso che, al posto di portarlo dove specificato, lo consegna a un centro di smistamento casuale. Nel centro di smistamento, viene rimosso il nostro indirizzo come mittente, messo quello del centro e inviato a un altro centro casuale. E da lì, una volta ricambiato l'indirizzo, a un altro ancora. Ecco, questi sono i 3 strati.  
L'ultimo centro spedirà infine il pacco alla famiglia Wikipedia che, contenta della sorpresa, decide di inviarci subito un pacco a sua volta. Come? Beh, il nostro indirizzo non lo sa. L'unica cosa che può fare è darlo al postino ancora lì davanti, cosicché lo riporti all'ultimo centro di smistamento. Il centro, che ha i registri degli ordini, lo invierà a quello prima, che lo invierà a quello prima, che... ha il nostro indirizzo e lo invierà a noi!  
Può sembrare buffo, ma è così che funziona internet, a pacchi (o meglio, a pacchetti). La rete non è altro che un esercito di postini digitali che fanno avanti e indietro per farci caricare il sito che abbiamo richiesto, il gioco online che abbiamo scaricato, il messaggio che abbiamo inviato. Tor non fa altro che aggiungere più passaggi nel mezzo, barattando una minor velocità per una maggior anonimia.  
  
A proposito di *maggior* anonimia, una domanda che salta fuori spesso è "quindi Tor è completamente anonimo?" e la risposta breve è no; è tuttavia ciò che si avvicina di più all'anonimato completo, in quanto richiede ingenti risorse governative per farlo vacillare (e di nuovo: se aveste un governo alle calcagna, non sareste qui a leggere). Un'altra domanda è "meglio VPN o Tor?". E, beh, per chi vuole approfondire questi due aspetti, ci abbiamo dedicato il "Tecnicamente".  
  
Ma alla fine di tutto ciò, quando usare Tor, e perché? Per il quando, ecco, pensate a quelle volte che aprite internet in modalità incognito, e sostituelo con Tor. Usatelo per masturbarvi, per esempio (i siti porno sono ricolmi di traccianti pubblicitari). O se abitate in regimi autoritari. Se la lentezza non vi turba⁴, usatelo in generale quanto possibile se non dovete entrare con qualche account, o comunque usatelo per ciò che ritenete più privato per voi - come appunto la sfera sessuale - se la rete è privata. NON usatelo su reti pubbliche (come quella scolastica, soprattutto se siete così cretini da inviare minacce di bombe perché volete saltare un esame⁵) perché anche se non si può sapere COSA fate su Tor, è comunque possibile sapere che lo state usando. 

## Cosa fare

[DA AGGIUNGERE]  
  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)

## Tecnicamente

[DA AGGIUNGERE: quanto è anonimo Tor, Tor vs VPN, Tor over VPN]  
  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)

## Appendice
¹ https://www.torproject.org/  
² Nd, [BBC News launches 'dark web' Tor mirror](https://www.bbc.com/news/technology-50150981), BBC, 2019  
³ Carola Frediani, [#Cybercrime](https://www.hoeplieditore.it/scheda-libro/frediani-carola/cybercrime-9788820389208-6127.html), Milano, 2019, HOEPLI; si veda il capitolo *Vita, morte e delirio dei mercati neri*  
⁴ preparatevi a venire inondati da ReCAPTCHA come se piovesse per dimostrare che non siete un robot  
⁵ Nd, [Harvard student Eldo Kim charged in final-exam bomb hoax](https://edition.cnn.com/2013/12/17/justice/massachusetts-harvard-hoax/), CNN, 2013