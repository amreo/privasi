# FAQ

#### Cos'è PrivaSì? 
PrivaSì è un percorso creato da un gruppo di ragazzi volontari (Etica Digitale), con l'intento di sensibilizzare le singole persone riguardo l'uso e abuso dei dati al giorno d'oggi. Tramite semplici spiegazioni, guide e, soprattutto, mostrando mezzi alternativi che rispettino la persona, si vuole dimostrare come si possa effettivamente riprendere in mano la propria privacy e, più in generale, la propria intimità.

#### Come posso sostenere il progetto?
In una maniera molto semplice ed economica: spargendo la voce. Non c'è cosa più utile, dato che è una cosa che riguarda tutti.  

#### Posso donare in qualche modo?
Sì, potete farlo tramite il sito LiberaPay [qui](https://it.liberapay.com/EticaDigitale/).  

#### Come posso contribuire alla documentazione?
Se volete aggiungere o correggere qualcosa e non sapete come funziona GitLab, il modo più veloce è farsi un account e postare qui il vostro suggerimento: [Issues](https://gitlab.com/etica-digitale/privasi/issues) (inseriremo presto un form da seguire). Per correttezza, dato che l'iscrizione a GitLab richiede un Captcha di Google e state capendo di cosa stiamo parlando, vi invitiamo a scaricare un browser usa e getta come [Iridium](https://iridiumbrowser.de/), da usare esclusivamente per registrarvi (e disinstallarlo). Se non avete capito... iscrivetevi e fine :D  
Per chi invece è proprio pratico con questi strumenti, forkate e fate merge request senza problemi :)
  
#### Perché la maggior parte delle fonti sono in inglese?
I contributi in lingua inglese sulla piazza sono più numerosi rispetto al materiale italiano, piuttosto scarno. Si tenga inoltre presente che spesso si usano board internazionali per seguire vari argomenti, dove gli articoli sono postati per l'appunto in inglese.  

#### Ho provato a seguire il percorso ma certe cose proprio non le capisco: come faccio?
Se certe cose vi risultano troppo complicate (nonostante cerchiamo di renderle più semplici possibili) chiedete una mano a persone voi care che se ne intendono: le nuove generazioni ci sono nate e cresciute in questo mondo, sicuramente sapranno aiutarvi. E se avete già passato il capitolo su Telegram, il gruppo esiste anche per questo :)  

#### Posso usufruire di PrivaSì per progetti personali?
Assolutamente sì. Come da licenza (GNU GPL 3.0, [consultabile qui](https://gitlab.com/etica-digitale/privasi/blob/master/LICENSE)), siete liberi di diffondere, copiare e modificare il progetto. Se cercherete di diffondere la vostra versione modificata però, anche la vostra versione dovrà essere sottoposta alle medesime condizioni. È gesto carino poi citare gli autori e (ma questa è una carineria per noi) farci sapere se state utilizzando PrivaSì per qualche progetto di divulgazione (per esempio scolastico). È sempre una bella cosa da sentire :)
<br>
<br>
<br>
[Torna all'homepage](https://gitlab.com/etica-digitale/privasi)
