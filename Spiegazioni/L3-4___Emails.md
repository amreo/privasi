# Email separate

> C'è un modo di evitare che i nostri account vengano associati alla stessa persona e individuare i servizi che condividono il nostro indirizzo email con altri e limitare lo spam? 

## In parole semplici

Spesso per usurfruire e registrarci a un sito web è necessario inserire il nostro indirizzo email che può essere usata per tracciarci anche sui altri servizi oppure condivisa con altri attori o peggio ancora pubblicata su internet in seguito a una fuga di dati (data breach). Come vi è stato detto in precedenza si possono usare dei indirizzi email temporanei ma non sempre questa soluzione va bene. Ad esempio noi vorremmo ricevere le notifiche del servizio oppure avere la possibilita di fare il recupero della password. La soluzione è quella di avere più indirizzi email separati, che siano separati fisicamente o virtualmente.
L'idea è quella di associare un indirizzo email univoco per ogni servizio, quando possibile.

## Cosa fare

### Linee generali

In linea generali ci sono tre modi per separare le email.
* Il primo modo è quello di creare un nuova registrazione per un account email da zero. Questa soluzione ha il vantaggio di essere solitamente gratuita e di garantire l'assoluta separatezza degli indirizzi email ma ha lo svantaggio che richiede la riconfigurazione del nostro client di posta. Inoltre la creazione di nuovi indirizzi email da zero è solitamente lunga e noiosa.
* Il secondo modo è quello di usare una funzionalità del fornitore di un indirizzo email dal nome come "alias" o "addresses" come permette sostanzialmente di associare più indirizzi email diversi dello stesso fornire alla stessa casella di posta elettronica. Il vantaggio è che solitamente è semplice e veloce creare nuovi alias con un nome arbitrario. Lo svantaggio è che solitamente questa funzionalità è considerata premium quindi ha un limite sul numero di alias nei vari piani tariffari. 
* Il terzo modo è quello di aggiungere un tag all'interno dell'indirizzo email. Questo modo è il più semplice e veloce ma ha il forte svantaggio che è sempre facile ricondurre all'indirizzo email """nascosto""" e quindi non è in grado di garantire la privacy soprattuto se il sito web conosce questo trucchetto. In alcuni casi i siti web non permettono l'inserimento di indirizzi email con tag perché credono che erroneamente il simbolo più non è un carattere che può essere all'interno di un indirizzo email.

### Come aggiungere il tag a un indirizzo email
Aggiungere un tag ad un indirizzo email è molto semplice. Basta mettere il carattere `+` seguito da una parola prima della chiocciola. Ad esempio se il nostro indirizzo email è `mario.rossi@eticadigitale.it`, un ipotetico indirizzo email con tag può essere `mario.rossi+vattalapesca@eticadigitale.it`.

### Come creare un alias
Le modalità per la creazioni di alias non sono le stesse tra i fornitori, per cui è consigliato andare a leggere la documentazione specifica del fornitore. Ad esempio su Protonmail gli alias(che protonmail chiama addresses) si creano in [questo modo](https://protonmail.com/support/knowledge-base/addresses-and-aliases/) invece su Tutanote si fa in [questo altro modo](https://tutanota.com/howto#alias)