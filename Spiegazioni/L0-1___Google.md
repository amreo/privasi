# Google - un esempio: vedere con i propri occhi i dati che abbiamo acconsentito di condividere

Questa parte vuole mostrare quante informazioni si condividono ogni giorno online, usando Google come esempio. Certo, è cosa nota che la pubblicità che appare rispecchia le ricerche o i discorsi di un individuo, ma da dove inizia questa raccolta dati? Partiamo da una semplice chiamata tra Tizio e Caio (la fantasia è di casa) filtrata negli occhi dell'azienda, e proviamo a rispondere a questa domanda: "quante volte è stato sbloccato il telefono per ottenere le varie informazioni?".
  
**Ingredienti**: un telefono Android senza modifiche; connessione dati attiva; GPS attivo.  
  
**Il caso**: Tizio chiama Caio per fare un giro.  
  
**Cos'è successo**: Caio ha risposto al telefono e accettato l'invito. Si sono trovati in un bar, fatto due passi per il centro visitando qualche negozio e verso sera si sono salutati.  
  
**Cos'è successo secondo Google**: Tizio Rossi (così appare nella rubrica di Caio) ha chiamato alle 10:37 per 5 minuti e 7 secondi; alle 14:02 si sono diretti al Bar Tazzina in Via Garibaldi 9 impiegandoci 17 minuti a piedi e passando per Via Mazzini, Via dei Fiori e Via Leonardo; sono rimasti fino alle 14:45 al bar, andando poi in centro e impiegandoci 20 minuti in macchina; parcheggiato, 7 minuti a piedi per arrivare in piazza, visitato Bricolage & Co. alle 15:25, Gelateria del Corso alle 16:10 e ImmobiliXTutti alle 17:00; alle 17:21 sono tornati alla macchina, arrivando a casa alle loro corrispettive case.  

Quante volte quindi è stato sbloccato il telefono per fargli sapere tutto ciò? Tutte le volte che sono entrati in un negozio? Tutte le volte che sono saliti in macchina? Magari solo un paio di volte durante la giornata. Sotto qualche riga c'è la soluzione.  
<br>
<br>
<br>
La risposta giusta è zero. Non è *mai* stato sbloccato. Sempre in tasca, niente app aperte, niente messaggi, niente ricerche, niente selfie, niente di niente: è lo stesso Google a dirlo.  
Potete infatti trovare qui le vostre attività: https://myactivity.google.com/myactivity  e qui i vostri spostamenti, se non avete negato il permesso al GPS: https://www.google.com/maps/timeline.
  
Se abbiamo attirato la vostra attenzione, dal Livello 1 vedremo come cancellare questi dati e negare questi permessi. Ma prima, capiamo cos'è successo.
  
## Cos'è successo
Quello a cui abbiamo assistito si chiama *profilazione*. In altre parole, si cercano di ottenere più informazioni possibili sul consumatore per avere un'idea sempre più precisa su chi sia - solitamente per scopi pubblicitari, ma non mancano casi più seri per influenzare voti elettorali. In altre parole, si accetta di raccontare tutto della propria vita in cambio di pubblicità più accurata. E questo non è un vantaggio, non per le persone almeno. No, il vantaggio è utilizzare programmi gratuiti (che hanno lo scopo di raccogliere ancora più dati), nella convinzione che qualcuno abbia deciso di regalarli. Pensateci un attimo: pagate per Gmail? Per Google Foto? Per Google Drive? Google Maps? Ancora meglio, c'è un solo servizio Google per il quale pagate di base? Cosa succederebbe invece se non pagaste per la luce, per internet, per l'acqua o per l'immondizia?<br>
Google non è una no-profit, anche i suoi dipendenti devono mangiare. E lo stesso discorso vale per Facebook, Instagram, Snapchat e qualsiasi altro servizio gratuito con un modello di business incentrato sulla profilazione.  
Non è quindi un cieco accanirsi sull'azienda, bensì sul modello di business al quale Google ha dato vita già nei primi anni 2000 e che ha preso sempre più piede. Un modello che considera le persone non come persone ma come numeri su cui fare incetta di dati in ogni singolo istante. E chi gli sta attorno poi inizia a imitarli: si pensi alle testate giornalistiche e a come i titoli siano sempre più acchiappa-click. Questo perché si è barattata la qualità per la quantità, dove generare traffico per arrotondare (soldi spartiti con Google in cambio di profilazione) è per alcune testate più importante di informare. Risultato? Minore fiducia nei giornali (la gente non è stupida), minore informazione di qualità, più ignoranza¹. A voi l'immaginare i danni sul lungo corso.  
  
Attenzione, inoltre, a pensare che ciò esista solo su prodotti gratuiti: un caso è il pacchetto Microsoft Office 365 che, grazie a un'inchiesta del governo olandese, è risultato tracciare dati sensibili come e-mail scritte dentro i suoi programmi (Word, Excel ecc.)²  
  
Per concludere, queste aziende vengono definite *monopoli d'informazione*, *capitalismo della sorveglianza*³: vivono dei nostri comportamenti e della nostra attenzione, convertendo questi dati grezzi in raffinati per accumulare conoscenza e capitale a spese di tutti gli altri. In un mondo dove poche persone sanno tutto del mondo, e dove il mondo non sa nulla di loro.  
  
Qui riportiamo i monopoli del 2012: la mappa indica il sito più usato in ogni stato⁴.  
![monopoli informazione](resources/L0-1_pic0.png)  
  
<br>
[Livello 1: "Rimozione Consenso"](https://gitlab.com/etica-digitale/privasi/blob/master/Spiegazioni/L1-0___Activity-Deletion.md)  
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹ Nalbone Daniele, Puliafito Alberto, [*Slow Journalism. Chi ha ucciso il giornalismo?*](http://www.fandangolibri.it/prodotto/slow-journalism/), Roma, 2019, Fandango Libri  
² Cimpanu Catalin, [*Dutch government report says Microsoft Office telemetry collection breaks GDPR*](https://www.zdnet.com/article/dutch-government-report-says-microsoft-office-telemetry-collection-breaks-gdpr/), ZDNet, 2018  
³ Shoshana Zuboff, [*Il capitalismo della sorveglianza*](https://www.luissuniversitypress.it/pubblicazioni/il-capitalismo-della-sorveglianza), Milano, 2019, LUISS  
⁴ Adiklis Vytautas, https://visual.ly/community/infographic/technology/world-map-dominating-websites, 2012
