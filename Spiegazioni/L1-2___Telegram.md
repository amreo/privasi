# Conversazioni di tutti i giorni: ~~Messenger~~ Telegram
> È nato prima l'uovo o la gallina? Allo stesso modo, è meglio Whatsapp o Telegram? Beh, c'è gente che ha litigato per molto meno. La risposta è, semplicemente, che son due app diverse e avrebbe più senso paragonare Whatsapp con app come Signal (come vedremo nel prossimo capitolo) o Wire. Quindi, se proprio vogliamo fare un paragone, prendiamo un'altra app molto famosa: Facebook Messenger. È meglio Messenger o Telegram? 

## In parole semplici

Partiamo dalla cosa più banale: Messenger appartiene a Facebook.  

Vi sarà sicuramente capitato in questi ultimi anni di sentire che il social di Zuckerberg stesse avendo problemi con la privacy. Il motivo è semplice: Facebook, oltre a condividere lo stesso modello di business di Google, si è dimostrato irresponsabile nel tutelare i dati raccolti. Questi dati sono infatti finiti nelle mani di aziende come Cambridge Analytica, che li ha poi utilizzati per manipolare elezioni politiche tramite annunci pubblicitari mirati e personalizzati¹.
  
Parlando nello specifico di Messenger, un'inchiesta del New York Times ha dimostrato come Facebook abbia dato a Spotify, Netflix e alla Royal Bank of Canada il permesso di leggere, scrivere e cancellare i messaggi privati degli utenti². Al di fuori di uno scarica barile dove nessuno sapeva niente, il problema resta: qualcuno aveva accesso alle conversazioni private di qualsiasi utente, con inoltre la possibilità di modificarle e immedesimarsi in loro. Non importa se poi lo hanno fatto o no (potremmo speculare all'infinito senza avere una risposta), importa che era possibile farlo.  
Sempre parlando di terze parti col permesso di guardare la chat, fino a inizio agosto 2019 gli audio inviati su Messenger erano affidati a terzi per essere trascritti, senza il benché minimo avviso all'utente³. In parole semplici, c'erano persone pagate per ascoltare gli audio inviati in chat. Ironico, considerato che tre mesi prima Zuckerberg affermò: "Il futuro è privato"⁴. E ci si potrebbe anche chiedere se avrebbero smesso di farlo se non li avessero scoperti (che è quello che è successo).  

Questo apre una riflessione sulla fiducia: come possiamo verificare che qualcuno faccia ciò che dice? La parola chiave è *open source*, ovvero mostrare pubblicamente ogni ingranaggio che fa funzionare un programma/sito ecc., così chi se ne intende può dirci se ci sono tiri mancini o meno (senza contare che possono aiutare a migliorare il prodotto). Nella pratica vuol dire pubblicare il *codice sorgente* e verificare (sempre chi se ne intende) che l'app che gira sul nostro dispositivo combaci con il codice pubblicato online.  

***Open Source*** (pronunciato "open sors") sarà la prima parola chiave di tutto il percorso: posso verificare che non ci siano fregature? Bene. Non posso? Male.

"Male" perché per quanto Zuckerberg (e chiunque altro) possa garantirci che l'app fa solo X e Y, o che il futuro è privato, l'unica scelta che abbiamo è quella di credergli o meno. In questo caso dovremmo fidarci di qualcuno che ha lasciato trapelare i dati di 87 *milioni* d'utenti con Cambridge Analytica, che ha fatto della profilazione il suo modello di business e che ha posizioni discutibili sull'etica di narrare la verità con gli annunci pubblicitari -anche se possono minare la democrazia di un paese-, purché permettano di far fare soldi all'azienda⁵.  

Quindi qual è un'alternativa *open source* per Messenger? Intanto capiamone la sua funzione: possiamo dire che Messenger sia un'app per chattare con persone alle quali non vogliamo dare il numero. Tra le alternative più famose ci sono quindi WeChat, LINE, Telegram e Viber; ma tra queste, ce n'è solo una *open source*: Telegram ([qui il suo codice sorgente](https://telegram.org/apps#source-code)).  
Per essere precisi solo una parte è *open* (spiegato nel dettaglio in "Tecnicamente"), ma per ora vi basterà evitare argomenti sensibili per non avere problemi :)

## Cosa fare
Scarichiamo l'app di Telegram dallo store del nostro telefono e, una volta completato il download, apriamola. Partirà un'installazione guidata dove ci verrà chiesto il nostro numero di telefono e un nickname. Fatto ciò, siamo pronti per partire.

Inoltre, dato che non vogliamo mostrare il nostro numero alle persone a cui abbiamo dato il nick, clicchiamo le tre lineette in alto a sinistra e nel menù a scomparsa premiamo su "Impostazioni".  
Lì andiamo su Privacy e Sicurezza e, premendo su Numero di telefono, mettiamo "Nessuno". Le altre opzioni invece impostatele a vostra discrezione.  

Torniamo poi indietro alle Impostazioni e, se vogliamo impostare un'immagine profilo, clicchiamo sull'icona della fotocamera. Potremo scegliere se scattarla sul momento o caricarla dalla galleria.  
<div align="center"><img src="resources/L1-2_pic0.png"></div>  

### Gruppo Telegram di Etica Digitale
Tra le tante funzioni di Telegram, c'è un sistema di ricerca per trovare persone, gruppi e canali (gruppi a senso unico dove si segue ciò che scrive il proprietario). Anche noi abbiamo pensato di fare un gruppo, dove siete tutti i benvenuti così da poter dare una mano e riflettere insieme sul percorso; o, più in generale, sull'etica digitale :)  
Per trovarlo, torniamo alla schermata principale e premiamo sulla lente d'ingrandimento.  
Si aprirà in automatico la barra di ricerca e lì scriviamo "etica digitale". Come primo risultato dovreste avere il nostro gruppo  
<div align="center"><img src="resources/L1-2_pic1.png"></div>  

Per entrare premeteci su: in basso noterete la scritta "Unisciti al gruppo". Premete e il gioco è fatto.  

### Regolamento
Si ricorda che, prima di scrivere, è opportuno leggere il regolamento. Lo trovate come "messaggio fissato" nella chat, ovvero nella parte alta dello schermo come da immagine. Se ci premete, vi porta al messaggio vero e proprio.  
<div align="center"><img src="resources/L1-2_pic2.png"></div>    
  
Prima di lasciarvi pasticciare (non nel gruppo ;) ma coi vostri amici ) con le varie funzioni di Telegram, è opportuno ricordare che il capitolo si chiama "Conversazioni di tutti i giorni". Questo perché vi **s**consigliamo di condividere informazioni troppo sensibili sull'app, per motivi spiegati in "Tecnicamente". Per quel tipo di informazioni, useremo Signal (c'è un capitolo apposito).  

### Versione Desktop
Se volete usarlo dal PC senza controllare ogni volta il telefono, Telegram ha anche una comodissima versione Desktop: la potete scaricare da [qui](https://desktop.telegram.org/)  

### Rimuovere Messenger
Ultimo ma non meno importante, disinstalliamo Messenger dal nostro telefono.  
Forse starete pensando che i vostri amici/familiari/colleghi/clienti sono lì e che non volete/potete lasciarli, ma rifletteteci un attimo: perché farsi del male se c'è una soluzione? Prima di tutto, se non potete proprio farne a meno, potete comunque continuare a usare la chat di Facebook dal computer. Questo è ottimo se avete clienti/datori di lavoro con i quali dovete rimanere in contatto e ai quali non volete dare il numero: perché fidatevi, la vostra sanità mentale non vuole che queste persone possano raggiungervi a qualsiasi ora del giorno o della notte sul telefono. Sul serio.    
Per gli amici e familiari il problema invece non si pone, perché si suppone abbiate i loro numeri e li sentiate già su app come WhatsApp o Signal.  
Tuttavia, c'è una soluzione ancora più semplice: perché non spiegare a queste persone quello che avete letto qui in alto? La privacy non è un insieme chiuso, funziona soltanto se anche le persone intorno a noi ne sono consapevoli. Che alla fine è il motivo per cui queste pagine esistono :)  
E perché poi, una volta che saranno su Telegram, il problema avrà smesso di esistere.  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Tecnicamente
Telegram si basa sul protocollo MTProto, creato ad hoc dagli sviluppatori di Telegram. Gli esperti di crittografia hanno criticato pesantemente questa scelta, poichè tipicamente essi preferiscono che venga utilizzato un protocollo già noto e ben analizzato, come ad esempio quello di Signal, anzichè creare il proprio. Ciononostante, MTProto finora si è rivelato essere tutto sommato sicuro: la crittografia dal client al server è sicura contro vari attacchi e intercettazioni nella rete locale, e anche la crittografia end to end è piuttosto sicura. L'unico possibile punto di vulnerabilità è il server di Telegram, poichè il protocollo assume che questo sia fidato per alcune funzioni: se il server fosse compromesso, qualcuno potrebbe inviare messaggi a nome nostro (ma questa sembra essere una possibilità molto remota). Va puntualizzato, in merito alla crittografia, che Telegram di default non utilizza la crittografia end to end, quindi i nostri messaggi saranno visibili al server, a meno che non utilizziamo una chat segreta, che invece è crittografata end to end.

Parlando del server di Telegram, è importante sottolineare che il server è proprietario, quindi nessuno può analizzarne la sicurezza, nè garantire che Telegram LLC non legga i nostri messaggi o li consegni alle autorità. L'azienda ha dichiarato di non farlo, e come risultato Telegram è stato bandito in diversi paesi (mentre Whatsapp, che non si fa problemi a collaborare con le autorità⁶, è disponibile ovunque), tuttavia non abbiamo una certezza fra le mani.

I client ufficiali di Telegram (PC, Mobile, Web) sono invece software libero (GNU GPLv3), come pure MTProto, quindi esistono client alternativi, ad esempio client da linea di comando per Linux. Il più importante è sicuramente Telegram FOSS, una versione modificata del client Android che rimuove qualsiasi funzione di dubbia privacy (analytics, dipendenze dai servizi google, blob binari, ecc.). Per questo motivo consigliamo fortemente di scaricare Telegram da F-Droid e non da Google Play, se siete su Android. E se non sapete di cosa stiamo parlando, non preoccupatevi: vi ci guideremo passo passo nel livello 2.

Detto questo, proprio per il server non open source e per la crittografia non abilitata di default (proprio come su Messenger), sconsigliamo di inviare dati sensibili o parlare troppo della vostra vita privata.  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹ Consigliamo la visione di [The Great Hack](https://www.thegreathack.com/) per approfondire l'argomento  
² Dance Gabriel J.X., LaForgia Michael, Confessore Nicholas, [As Facebook Raised a Privacy Wall, It Carved an Opening for Tech Giants](https://www.nytimes.com/2018/12/18/technology/facebook-privacy.html), The New York Times, 2018  
³ Frier Sarah, [Facebook Paid Contractors to Transcribe Users’ Audio Chats](https://www.bloomberg.com/news/articles/2019-08-13/facebook-paid-hundreds-of-contractors-to-transcribe-users-audio), Bloomberg, 2019  
⁴ Statt Nick, [Facebook CEO Mark Zuckerberg says the ‘future is private’](https://www.theverge.com/2019/4/30/18524188/facebook-f8-keynote-mark-zuckerberg-privacy-future-2019), The Verge, 2019  
⁵ Guardian News, ['So you won't take down lies?': Alexandria Ocasio-Cortez challenges Facebook CEO](https://invidio.us/watch?v=8KFQx-mc2Ao)(video), 2019  
⁶ nd, [Facebook e WhatsApp, accordo USA-UK per fornire i messaggi degli utenti alle autorità](https://www.hdblog.it/2019/09/30/facebook-whatsapp-crittografia-messaggi-autorita/), HD Blog, 2019
