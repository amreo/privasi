# WebApps: social a compartimenti stagni

>  Toccando un tasto dolente come i social network, vediamo cosa possiamo fare via telefono per ridurne i danni

## In parole semplici

Un argomento molto dibattuto e complicato da trattare quando si parla di privacy è la nostra presenza sui social network: per quanto veritieri e teoricamente efficaci, approcci quali "cancellati da X, è il male" non funzionano per il semplice motivo che X (sia esso Facebook, Instagram, Twitter, LinkedIn o Reddit) è ormai nel tessuto sociale. Non è per esempio difficile trovare istituzioni come le università che hanno gruppi Facebook per ogni annata, usati per comunicazioni ufficiali e che rendeno di conseguenza la vita più difficile a chi non è sul social. O si pensi anche banalmente alla politica, che ci investe centinaia di migliaia di euro per farsi pubblicità¹. A questo possiamo poi sommare il meccanismo base dei social, progettati per monetizzare la nostra attenzione (motivo per il quale ci si ritrova a scrollare annoiati senza nanche capire il perché piuttosto che chiudere la pagina). Insomma, argomenti complessi vanno trattati in maniera complessa e non è questa l'occasione² ³.  

Quello su cui invece si può iniziare a lavorare è ridurre l'invasione di privacy di questi soggetti ed assumere comportamenti quali "non condividere proprio tutto con tutti"; che tradotto, significa da una parte bloccare la raccolta di più informazioni possibili da parte del social, e dall'altra essere più coscienziosi su quello che si posta e a chi lo si rende visibile.  

Per quanto sia subdolo, infatti, i colloqui di lavoro si sono spostati dagli uffici al proprio social: le persone che dovrebbero scrutinare i candidati sono pagate per guardare i vostri profili pubblici e farsi un'idea di chi siete⁴. Ci sono anche casi dove persone hanno perso il posto di lavoro per qualcosa che avevano postato 6 anni prima (magari da adolescenti), o casi dove giornalisti, pur di mettere sotto una cattiva luce qualcuno, hanno scavato per anni e anni di post fino a trovare qualcosa di controverso⁵. Insomma, i social non sono altro che registri di parti della nostra vita, e se tutto diventa pubblico, diventa un'arma a doppio taglio.

Cosa fare invece per limitare il social stesso? Sul computer ci abbiamo già pensato con Firefox e i vari add-on, il problema rimane il telefono. O forse no, peché esiste WebApps :)

Per capire come funziona quest'app, immaginate di avere tre persone e tre stanze NON comunicanti tra loro. In ogni stanza c'è una persona: se entrate nella prima stanza e date una pallina blu a chi c'è dentro, possono le altre due sapere che gli avete dato una pallina o, addirittura, di che colore è? A meno che non siano supereroi con vista a raggi X (ma qui andiamo nel regno dell'assurdo :D) no, perché appunto non sono comunicanti: complimenti, avete appena creato dei compartimenti stagni!  

WebApps fa esattamente questo: separa ciò che fate su un social da tutto il resto. Lo chiude in una stanza - o meglio, in un contenitore -, cosicché non vada a ficcanasare dove non dovrebbe.  

Quindi forza e coraggio, tempo di ridimensionare certi spazi :)

## Cosa fare

### Linee generali

Iniziamo dalle cose di base. Se volete evitare che sconosciuti si facciano troppo gli affari vostri ma non volete/potete allontanarvi dal social, non avete bisogno di un'app. Le soluzioni migliori sono banalmente due:
- non postate tutto ciò che fate o che vi passa per la testa. Prima di postare qualcosa, contate fino a 10 e chiedetevi: "è davvero una cosa che voglio condividere col mondo o è qualcosa che voglio teneremi per me e qualche amico?"
- non tenete un profilo pubblico (Instagram) e/o non mettete tutti i post pubblici (Facebook). Perché, d'altronde, degli sconosciuti devono sapere tutto di voi?

### WebApps

Andiamo su F-Droid, scarichiamo l'app - nome completo WebApps Sandboxed Browser - e installiamola.  
Alla prima apertura ci spiegherà le sue funzioni base, ma cercheremo di renderle ancora più semplici qui.  

Di base l'app ha già dei siti aggiunti come DuckDuckGo, Facebook e Google News. Iniziamo a sbarazzarci di quelli che non ci servono tenendo premuto sul sito in questione e selezionando "Ok" alla domanda "Cancellare WebApp?". DuckDuckGo non serve sicuramente (già lo abbiamo su Firefox Klar), Hacker News se non si sa l'inglese e non ci si interessa di hacking neanche. Il resto dipende da che siti usate. Se non siete sicuri, cancellate :D  

<div align="center"><img src="resources/L2-6_gif0.gif"></div>  

#### Impostare un sito su WebApps

Dato che il meccanismo è lo stesso per tutti i social/siti, useremo Facebook come esempio. Premiamo sulla sua icona.  

La cosa fondamentale da ricordarsi è questa: essendo un contenitore, di base WebApps blocca qualsiasi richiesta esterna. Tuttavia, dato che molti social hanno bisogno di comunicare con certi componenti extra per funzionare correttamente (Facebook ne ha uno per essere visualizzato bene), dobbiamo andarli ad abilitare. In questo caso il componente che ci serve si chiama ```fbcdn.net```. Può sembrare arabo quindi passiamo semplicemente alla pratica :D  

Premiamo sulle due freccette in alto per vedere la lista dei componenti, dalla lista selezioniamo fbcdn.net e poi "Sblocca". Facebook funziona e non dobbiamo più metterci mano!

<div align="center"><img src="resources/L2-6_gif1.gif"></div>  

Segue qui una lista dei componenti da abilitare per i social più famosi:
- **Reddit**: redd.it, reddit.com, redditmedia.com, redditstatic.com
- **Instagram**: [DA INSERIRE]
- **Twitter**: twimg.com

#### Icona sulla home

Ora non ci resta che mettere Facebook a portata di click. Insomma: non vogliamo aprire WebApps ogni volta E POI aprire Facebook da lì.  
Con Facebook ancora aperto, premiamo sui 3 puntini in alto a destra e selezioniamo "Aggiungi a schermata Home". Ci chiederà una conferma e *tadaan*, il social ha ora la sua iconcina sulla schermata principale.  

<div align="center"><img src="resources/L2-6_gif2.gif"></div>  

#### Aggiungere un nuovo sito

Mettiamo però che il sito che vogliamo non sia nella lista iniziale. Per aggiungerlo apriamo WebApps e premiamo sul pianetino in alto: digitiamo il sito (per esempio ```instagram.com```), confermiamo e nel sito che si apre andiamo sui 3 puntini. Da lì selezioniamo "Salva come Webapp" e il gioco è fatto.   

<div align="center"><img src="resources/L2-6_gif3.gif"></div>  

Prima di concludere, dobbiamo avvisare di due cose importanti:
- **Se un sito funziona male, non è colpa di WebApps**: quello che fa l'app è mostrare la versione mobile del social/sito, né più né meno. Per esempio, i link nelle storie di Instagram non funzionano, ma questo è un problema dovuto *a Instagram*.
- **WebApps non vi invierà notifiche**: questo vuol dire che richieste d'amicizia, commenti e messaggi privati NON saranno notificati sul vostro telefono a meno che non apriate il sito in questione.

Per ovviare a quest'ultimo problema e se avete le app di quei social installate, invitiamo a un approccio progressivo: tenete in un primo periodo le app dei social sul vostro telefono, e iniziate prendendo l'abitudine di aprirli tramite WebApps. Se le notifiche sono di fondamentale importanza per voi (magari per lavoro, anche se è sempre meglio non portare il lavoro sui social), predefinitevi un orario dove controllate ogni giorno. Lo aprite, controllate, fate eventualmente quello che dovete fare (possibilmente che non sia scrollare all'infinito per noia) e fine. Fatelo per due settimane in modo da abituarvici e infine, disinstallate quelle app. In questo modo avrete ridotto il "rumore" di scuse per farvi controllare il telefono che, ve lo garantiamo, vi permetterà di vivere un po' meglio.  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  


## Tecnicamente

[DA AGGIUNGERE?]  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  


## Appendice
¹ https://www.reddit.com/r/italy/comments/e0kdqa/la_pagina_fb_di_salvini_ha_speso_al_momento/; i dati si riferiscono al periodo marzo - 21 novembre 2019 e sono presi dal servizio ufficiale di Facebook che comunica le spese. Al contrario del titolo per fare click ("Salvini! Salvini!"), le spese dei partiti + esponente sono simili. 165.000 per la Lega, seguita dai 149.000 del PD e dai 138.000 di Forza Italia.  
² Dato che se n'è accennato, si consiglia comunque a persone di rilevante peso nelle istituzioni (come le università) di far leva per evitare di portare aspetti pubblici (come l'istruzione) su un privato che vive di pubblicità. Non svendete certe cose per una comodità ad apparente costo zero  
³ Si pensa infatti di fare in futuro un percorso sulla FOMO (*Fear of Missing Out*, paura di essere tagliati fuori), un fenomeno che vede nei social un aspetto molto importante - e controproducente  
⁴ nd, [More Than Half of Employers Have Found Content on Social Media That Caused Them NOT to Hire a Candidate, According to Recent CareerBuilder Survey ](https://www.prnewswire.com/news-releases/more-than-half-of-employers-have-found-content-on-social-media-that-caused-them-not-to-hire-a-candidate-according-to-recent-careerbuilder-survey-300694437.html), PR Newswire, 2018  
⁵ Lynn Samara, [Viral 'beer money' fundraiser erupts into racist tweets, a fired reporter and online drama](https://abcnews.go.com/US/racist-tweets-backlash-drama-surround-viral-beer-money/story?id=65849942), ABC News, 2019
