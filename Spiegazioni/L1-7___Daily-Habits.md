# Gesti quotidiani

> Ora che conosciamo gli strumenti base per muoverci, vediamo come implementare delle piccole abitudini per evitare ulteriormente il diffondersi abusivo dei nostri dati. Come tutte le cose, anche la privacy nasce da piccoli gesti.

## In parole semplici  

Avete presente di quando si parlava di come avere la porta blindata e le finestre scoperte fosse stupido? Bene, tempo di chiudere le finestre.

## Cosa fare

*  **WiFi (telefono)**: ricordate di disattivarlo prima di uscire di casa. Vi consigliamo di connettervi solo a reti che considerate sicure, tipo quelle di un vostro amico o parente. Evitate assolutamente i WiFi negli aeroporti, in quanto gli aeroporti sono il primo luogo di spionaggio industriale¹. Allo stesso modo evitate quelli che vi richiedono una registrazione. Comunque, i piani telefonici ormai offrono fino a 40-50GB al mese, rendendo superfluo il connettersi ai WiFi pubblici a meno che non ci sia campo.
*  **Caricatori pubblici**: Se non potete assicurarvi di usare il vostro caricatore e di inserirlo dentro una normale presa, non usateli². Manomettere una presa USB non è impossibile, e usufruirne non farà che infettare anche il vostro telefono. Il consiglio migliore in assoluto è quello di portarvi una power bank con voi quando dovete affrontare viaggi lunghi, onde evitare rischi.
* **Bluetooth**: se non vi serve, spegnetelo. I dispositivi hanno un'impronta digitale unica legata al bluetooth, e per ottenerla basta trovarvi con il bluetooth accesso. Non regaliamo impronte digitali!  
* **GPS**: non regaliamo più dati del previsto ad app che utilizzano il GPS: come sopra, se non lo usiamo, spegniamolo.
* **Carte fedeltà**: a meno che non siate compratori compulsivi o compriate sempre nello stesso negozio, le carte fedeltà non vi servono a nulla se non a spingervi a comprare di più (nei casinò sono una vera piaga per spremere il giocatore³). Anche se le fate senza aderire alla parte pubblicitaria, sono comunque vostri dati salvati sull'ennesimo server. E se sono riusciti a bucare le carte fedeltà della Mastercard...⁴
* **Mail sospette**: se non siete sicuri del contenuto di una mail, cancellatela senza cliccare da nessuna parte e senza scaricare allegati (se non volete far la fine dei democratici alle elezioni americane del 2016⁵). Un'ulteriore verifica è quella di controllare l'indirizzo del mittente: se per esempio il titolo dice "Hai un pacco Amazon da ritirare!" e viene da nomestrano@indirizzomaisentito.com, state pur sicuri che l'unico pacco presente è la fregatura che vogliono rifilarvi. Ovviamente non credete neanche a nessuno che voglia regalarvi dei soldi dal nulla.  
* **Aggiornamenti**: se un programma vi chiede un aggiornamento, toglietevi il vizio di cliccare su "più tardi" perché probabilmente quell'aggiornamento ha (anche) sistemato delle falle di sicurezza. Il virus WannaCry ha messo in ginocchio mezzo mondo perché la gente non aveva aggiornato i sistemi operativi di Windows 7⁶.

E infine la regola numero 1: se non è necessario, non metterlo su internet (o su un apparecchio ad esso collegato, come il telefono).  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice

¹ Suc Mathieu, [L’aéroport, ce nid d’espions](https://www.mediapart.fr/journal/international/130719/l-aeroport-ce-nid-d-espions), Médiapart, 2019  
² Biersdorfer J.D., [The Risk in Using a Public Phone Charger](https://www.nytimes.com/2017/05/10/technology/personaltech/the-risk-in-using-a-public-phone-charger.html), The New York Times, 2017  
³ Long Emily, [How Casinos Use Rewards Programs to Track Everything You Do](https://lifehacker.com/how-casinos-use-rewards-programs-to-track-everything-yo-1830864482), Lifehacker, 2018  
⁴ nd, [Mastercard loyalty programme customers hit by data leak](https://www.finextra.com/newsarticle/34310/mastercard-loyalty-programme-customers-hit-by-data-leak), Finextra, 2019  
⁵ Frediani Carola, [#CYBERCRIME - Attacchi globali conseguenze locali](https://www.hoepli.it/libro/cybercrime/9788820389208.html), HOEPLI, Milano, 2019 (vedere capitolo 2: *L'amo nella posta dei democratici*)  
⁶ ibid, capitolo 1: *L'eroe di WannaCry*  