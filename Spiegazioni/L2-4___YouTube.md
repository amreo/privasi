# ~~YouTube~~ Video in un riflesso

> ATTENZIONE: questo capitolo è da riscrivere in parte in quanto invidio.us è ora un sito che rimanda a più istanze (di Invidious)

> C'è un modo per evitare la profilazione di YouTube? E quant'è cambiata la piattaforma in questi anni? Alla scoperta dei *mirror*

## In parole semplici

Per questo capitolo adotteremo un approccio diverso: vi abbiamo già raccontato pienamente di come funzioni Google (e YouTube appartiene a Google dal 2006), quindi dibattere per l'ennesima volta sulla privacy e i suoi modi per fare soldi ci pare superfluo. Parliamo, invece, semplicemente di YouTube.  

Il sito fu creato nel 2005 da tre impiegati PayPal e quello che offriva era semplice: uno spazio per condividere video. Non c'erano i pollici per votare, bensì le stelline (da 1 a 5), la gente si arrangiava con ciò che aveva in casa e fare lo "YouTuber" come lavoro non era neanche immaginabile. La gente lo faceva per il gusto di fare, per esprimersi, per mettersi in contatto con altre persone, ma ancora di più per costruire una realtà più "personale" che si distaccasse dalla TV.  

Con il passare del tempo, YouTube è inevitabilmente cambiato. Sono emersi i manager, fino a che YouTube non è diventato in alcuni casi una piattaforma di lancio per finire in TV o sul grande schermo¹. E va bene, questo alla fine non è dipeso dalla piattaforma.  
Quello che invece *è* dipeso dalla piattaforma, è come YouTube in questi ultimi anni abbia iniziato a prediligere i canali di emittenti televisive, scordandosi dell'utenza che lo aveva reso ciò che era. Sono noti più casi dove la piattaforma ha trattato con due pesi due misure le emittenti e gli YouTuber che parlavano del medesimo argomento, demonetizzando questi ultimi: per esempio quando il conduttore del Jimmy Kimmel Live (show televisivo della ABC) parlò delle sparatoria avvenuta a Las Vegas nel 2017 andò tutto bene², ma quando ne parlò lo YouTuber Casey Neistat aprendo addirittura un fondo per le vittime, venne privato da YouTube della possibilità di guadagnare soldi su quel video³.  

Ulteriore testimonianza di questo risvolto sono gli ultimi YouTube Rewind: gli YouTube Rewind sono video che dovrebbero riassumere gli eventi clou successi in un anno sulla piattaforma. La community si è sentita sempre meno rappresentata dai Rewind, dove sono apparsi personaggi della TV come Will Smith, John Oliver e Trevor Noah⁴. Inoltre, come evidenziato dagli YouTuber RackaRacka, YouTube è diventato sì "disneyano", ma al tempo stesso produce show come Wayne che vanno contro a tutte le policy che tanto vuol fare rispettare a suon di demonetizzazione⁵ (scene violente ed esplicite, imprecazioni continue).

E quest'arma che YouTube usa, la demonetizzazione (ovvero non permettere di guadagnare soldi su un determinato video), è diventata un incubo per molti YouTuber che hanno timore di parlare di certi argomenti per paura di farla scattare. La cosa più grave è che, una volta demonetizzato un video *o addirittura l'intero canale*, YouTube non dà mai una motivazione precisa se non che "ha violato gli standard della community". È come venire accusati di un reato ma senza dirvi quale.  

Insomma: le cose cambiano, certo, ma nel corso degli anni YouTube è diventato esattamente quell'ambiente dal quale si voleva scappare: la TV e la sua censura. Che collabora con la TV e che produce i suoi show originali, trattando con poco riguardo (e ingiustizia) quei creatori che hanno contribuito a far diventare popolare la piattaforma. Quest'ultimi, per arrangiarsi hanno aperto donazioni su siti esterni proprio per evitare le ansie della demonetizzazione, come Patreon o Ko-Fi. Ed è infatti diventato più efficiente supportarli da lì che con una visualizzazione.  

Cos'è quindi più etico fare? Venire profilati e contribuire a un sistema che non ricompensa neanche più i suoi creatori o continuarne egoisticamente ad usufruirne evitando sia la profilazione sia il supportare queste persone che faticano sempre di più? La risposta più sensata è in verità costruire delle alternative per far sì che questi creatori abbandonino YouTube; alternative che già esistono e che tratteremo in "Approfondendo".

Quello che possiamo invece fare già da oggi e senza il minimo sforzo è guardare i video di YouTube da dei *mirror* (letteralmente "specchi"). Tra questi servizi non ufficiali (siti o app) ce ne sono alcuni che spogliano i video da ogni mezzo di profilazione. Per quanto riguarda supportare gli YouTuber, invece, tenete in considerazione che supportarli equivale a incentivarli nel continuare a fare video sulla stessa piattaforma che non rispetta né loro né il pubblico. Perché allora, non parlare loro di quelle alternative che vedremo in fondo al capitolo?

## Cosa fare

#### NewPipe

NewPipe è un'app disponibile su F-Droid. La schermata principale si suddivide in Tendenze, Iscrizioni e Playlist salvate, e offre addirittura più funzionalità di YouTube stesso. Per esempio, potete riprodurre un video in sottofondo senza dover tenere lo schermo acceso (come una canzone). Per farlo vi basta tenere premuto su qualsiasi video e selezionare "Play directly in background" (riproduci direttamente in sottofondo).  

Come YouTube, inoltre, permette di iscriversi ai canali; e non è richiesto nessun account! Se siete pratici e siete iscritti a tanti canali, potete esportare da YouTube le vostre iscrizioni come spiegato [qui](https://www.addictivetips.com/web/export-youtube-subscriptions/) e importare il file nella sezione "Iscrizioni" dell'app, sennò cercate semplicemente i vari canali da NewPipe e iscrivetevi da lì.  

Unica cosa da fare presente è che in nessuno dei mirror vi sarà permesso mettere mi piace e commentare, per un semplice motivo: sono funzioni che richiedono per forza un account YouTube. Esclusa la valenza quasi pari a zero di "mi piace" e "non mi piace", potreste darvi alla corrispondenza via mail con il vostro YouTuber preferito (e apprezzerà di più perché è più "diretto"). O, se proprio avete bisogno di commentare, aprite il video da YouTube e aggiungete il commento.  

NewPipe apre sia i link YouTube che Invidious (spiegato qui sotto)

#### Invidious

Invidious è un sito (https://invidio.us), ed è ottimo per quando si vuole vedere un video da browser senza passare per YouTube. Se ci avete fatto caso, i video linkati nell'appendice di questo capitolo (e in generale in tutto il percorso) non portano a YouTube bensì a Invidious. E la cosa bella è che qualsiasi link YouTube è facilmente convertibile in un link Invidious: basta togliere "youtu.be", "youtube.com" o "m.youtube.com" dal link e sostituirlo con "invidio.us". Per esempio:

> https://youtube.com/watch?v=PuQt9N4Dsok  ==>  https://invidio.us/watch?v=PuQt9N4Dsok (cliccare per credere)

Quindi se ricevete un link YouTube da PC, vi basta applicare questo trucchetto per vederlo su Invidious :)  Inoltre se volete scaricare un video, vi basta far clic destro su di esso e "Salva video".
L'unica apparente limitazione è che di base il sito non permette video di qualità più alta di 720p (se son più alti li scala in automatico) e che canali certificati come VEVO o i "Topic" di YouTube non riescono a venire riprodotti. Tuttavia per rimediare a questi due problemi è sufficiente aggiungere ```&quality=dash``` in fondo all’URL del video. Esempio:

> https://invidio.us/watch?v=PuQt9N4Dsok  ==>  https://invidio.us/watch?v=PuQt9N4Dsok&quality=dash 

L'unica pecca è che in questo caso il video si caricherà più lentamente. 

#### FreeTube

FreeTube è un'applicazione desktop (PC) che usa la struttura di Invidious, ma senza limitazioni sulla qualità dei video e sulla riproduzione (come VEVO). Ed essendo un'applicazione, potete lanciare tutto dal computer senza aprire alcun browser. È disponibile per Windows, Mac e per molte distribuzioni Linux. Potete scaricarlo da [QUI](https://freetubeapp.io/#download).  

Come NewPipe, anche FreeTube permette le iscrizioni, che potete aggiungere sia manualmente che importandole da YouTube. E che, al contrario di NewPipe, vengono visualizzate proprio come su YouTube. Il design, inoltre, è stato pensato simile a YouTube per permettere una migliore transizione da uno all'altro. L'unica sua limitazione attuale è che non permette di visualizzare più di 20 commenti per video. Tra le tre, è senza ombra di dubbio la più completa.  
<br>  
<br>  

Complimenti, avete appena ridotto il vostro consumo di YouTube se non del 100%, almeno del 90% :D  

Come molte applicazioni *open source*, vi ricordiamo infine che donare agli sviluppatori -o aiutarli col codice se ne siete in grado- è un bel gesto, perché se non fosse per loro non avremmo queste alternative. Prendetevi un attimo e rifletteteci, perché anche un euro al mese fa la differenza (e se considerate che Spotify costa 10€/mese, quando potete ottenere una cosa simile con NewPipe creandovi delle playlist...).  
[Dona a NewPipe](https://liberapay.com/TeamNewPipe/) --- [Dona a Invidious](https://liberapay.com/omarroth) ---  [Dona a FreeTube](https://liberapay.com/FreeTube)

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Tecnicamente

[DA AGGIUNGERE: PeerTube]  

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  

## Appendice
¹ come lo spiacevole [Game Therapy](https://www.imdb.com/title/tt4421344/)  
² Jimmy Kimmel Live, [Jimmy Kimmel on Mass Shooting in Las Vegas](https://invidio.us/watch?v=ruYeBXudsds) (video), 2017  
³ CaseyNeistat, [LET'S HELP THE VICTIMS OF THE LAS VEGAS ATTACK](https://invidio.us/watch?v=tfZvSS_f254) (video), 2017  
⁴ YouTube, [YouTube Rewind 2018: Everyone Controls Rewind | #YouTubeRewind](https://invidio.us/watch?v=YbJOTdZBX1g) (video), 2018  
⁵ RackaRacka, [Youtube hates RackaRacka #freeracka](https://invidio.us/watch?v=q3blsxaw6bg) (video), 2019  
