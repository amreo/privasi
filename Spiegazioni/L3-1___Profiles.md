# Social e Professionale: profili Firefox

> Come imparare ad affacciarsi su finestre diverse senza dar via tutto di sé

## In parole semplici

Si è detto in precedenza che i browser (come Firefox) sono una finestra che si affaccia sul mondo digitale. Se dovessimo visualizzare questa finestra per come tendiamo a usare internet però, la casa sarebbe un grande cilindro vuoto e la finestra percorrerebbe a 360 gradi tutta la parete esterna. In altre parole, noi possiamo vedere tutto quello che c'è fuori senza muoverci, ma al tempo stesso anche chi è fuori può seguirci per tutta casa con altrettanta facilità.  
  
Nel mondo reale non troviamo molte case del genere, e per una buona ragione: d'altronde se possiamo ottenere lo stesso vantaggio di guardare fuori e al tempo stesso impedire di essere osservati a 360 gradi, perché allora non farlo (senza contare l'idea che cuoceremmo vivi d'estate)? Ed è infatti quello che facciamo, usando... beh, più finestre :) Più finestre di dimensione ridotta, che si affacciano su posti diversi: il cortile, la strada, la casa dei vicini. E il prezzo da pagare è pressoché nullo: basta cambiare stanza.

Quello che impareremo a fare in questo capitolo è smantellare questo super finestrone e trasformarlo in finestre più piccole che ci permettono di affacciarci sul mondo digitale, senza che quello che facciamo in salotto (per esempio sugli account social) sia associato con quello che facciamo in bagno (per esempio... ;) ).

## Cosa fare

Apriamo Firefox e nella barra di ricerca cerchiamo `about:profiles`, premendo poi invio.  
Questa è l'interfaccia dalla quale possiamo gestire le nostre finestre, ovvero i nostri profili. 

#### Quanti profili sono abbastanza?
Potete fare quanti profili volete - uno solo per la banca, uno per gli ospiti - ma la base è solitamente fatta da 3 profili: mondo professionale, mondo social e "altro". Questo perché non vogliamo che la nostra sfera scolastica/lavorativa, quella insomma che maneggia più dati sensibili di tutte, venga mischiata con i dati raccolti dai social o sia comunque abbinabile a cosa facciamo di solito su internet. Essendo la parte più sensibile, vogliamo tenerla ben isolata (a meno che il vostro lavoro non sia fare il social manager, e in quel caso vi consigliamo un profilo a parte ancora, facendo il possibile da WebApps quando siete al telefono).

Dato che non sappiamo se ci potrà mai servire un nuovo profilo, quello che faremo è crearne uno che faccia da matrice, cosicché qualora dovessimo sentirne il bisogno, potremo duplicarlo in una manciata di secondi.

#### Creazione matrice
Ora quello che avete usato fino ad adesso è... beh, un bel miscuglio. Premete quindi su "Nuovo Profilo" (in about:profiles) e seguite la procedura guidata. Quando vi chiederà un nome, mettete qualcosa come "CLONAMI - NON USARMI" e premendo "Fine" dovreste ritrovarvi con una cosa simile:  
  
<div align="center"><img src="resources/L3-1_pic0.jpg"></div>  

Fatto ciò, ecco... la parte non proprio divertente di questo capitolo: vi ricordate di quando abbiamo personalizzato Firefox per fare in modo che tutelasse la privacy? Diciamo che va rifatto - per l'ultima volta - sul nuovo profilo. Magari potreste pensare "e perché non ce l'avete fatto fare subito?", ma la risposta è semplice: il capitolo di Firefox era già lungo di suo e richiede tempo per abituarsi alle modifiche. Se avessimo trattato anche questo argomento, il rischio sarebbe stato quello di spaventare/disorientare più che di aiutare, che è l'ultima cosa che vogliamo.  

Cliccate quindi su "Avvia un nuovo browser con questo profilo" e seguite le [istruzioni del capitolo su Firefox](https://gitlab.com/etica-digitale/privasi/-/blob/master/Spiegazioni/L2-2___Firefox.md#pc-download-e-impostazioni-base).  
Inoltre, per non confondervi è buona cosa sapere che profilo state usando. Un modo semplice per farlo è creare una cartella nella barra dei segnalibri contenente il nome del profilo, come mostrato qui di seguito:

<div align="center"><img src="resources/L3-1_gif0.gif"></div>  

In questo modo vi basterà guardare l'angolo dello schermo per sapere sempre quale state usando :)  

#### Creazione nuovi profili

Ora che abbiamo la matrice bella che pronta, possiamo darci alla pazza gioia. Come esempio, procediamo a crearne uno che useremo esclusivamente per la nostra sfera professionale.  
Come prima, premiamo su "Nuovo Profilo" e chiamiamolo appunto "PROFESSIONALE".  
Una volta creato, aspettiamo un attimo prima di aprirlo, e dirigiamoci invece sul nostro "CLONAMI - NON USARMI" per copiarne le impostazioni. Come? Aprendo quella che chiama "cartella radice" e copiandone il contenuto, come da immagine.  

<div align="center"><img src="resources/L3-1_gif1.gif"></div>  

Come potete vedere, abbiamo poi aperto la cartella radice di "PROFESSIONALE", cancellato ciò che c'era e ne abbiamo incollato pari pari il contenuto. Questo perché la cartella radice è quella che contiene le informazioni che differenziano i vari profili. In parole povere, ci siamo creati una nuova finestra risprmiandoci la fatica di reimpostare tutti i vari settaggi.  

#### Aprire comodamente un profilo

Se per aprire un profilo specifico non volete ogni volta digitare about:profiles e andare a pescarvelo, vi mostriamo come creare delle icone sul desktop che vi aprano direttamente quello desiderato.  
Prima di tutto creiamo un'icona generica andando nel menu Start, cercando "firefox" e premendo col tasto destro sull'icona della volpe. Da lì selezioniamo l'opzione per creare una scorciatoia sul desktop.

> ATTENZIONE: la grafica e la scritta cambiano da computer a computer. Per esempio, su Windows 10 dovete prima premere su "Apri percorso file", trovare l'icona, cliccarla col destro e fare "Crea collegamento" (e vi chiederà di metterlo sul desktop). Insomma, non molto comodo. La GIF qui sotto è stata invece fatta su Linux Mint

<div align="center"><img src="resources/L3-1_gif2.gif"></div>  

Una volta creata, premiamoci su col tasto destro e, a seconda del sistema operativo, andiamo ad aggiungere `-P nomeprofilo` dove mostrato. Nel nostro caso, sarà -P PROFESSIONALE

<div align="center"><img src="resources/L3-1_pic1.jpg"> <img src="resources/L3-1_pic2.jpg"></div>  

Et voilà. Poi, per non confonderci, conviene sempre rinominare l'icona in qualcosa che ci dica di che profilo si tratta.  
Se poi in futuro si vorranno creare più icone associate a più profili, vi basterà copia-incollare questa e cambiare -P PROFESSIONALE con il nome corrispondente.  
<br>  
  
Ora che siamo giunti alla fine del capitolo, vi lasciamo con un "compito": dato che non dovreste usare il profilo professionale per navigare normalmente su internet, provate a crearvene uno da soli - seguendo gli stessi passaggi - per la navigazione standard (e ricordiamo che i social necessiterebbero di un profilo a sé stante, perché non avete idea di quanti dati collezionino). E, dato che deve essere lo standard, ricordatevi di impostarlo come predefinito una volta creato premendo il tasto apposito! (di norma ogni volta che create un nuovo profilo diventa quello predefinito, quindi ricordatevi di cambiarlo)  

<div align="center"><img src="resources/L3-1_pic3.jpg"></div>  

Infine, noterete una cosa: che se sbloccate un sito con uBlock Origin, non verrà sbloccato automaticamente negli altri profili. Questo appunto perché funzionano da compartimenti stagni e ciò che fate da una parte non influisce su ciò che fate dall'altra. Semplicemente, per precauzione, evitate di tenere aperti gli stessi siti su due profili in contemporanea, e soprattutto evitate di fare l'accesso allo stesso sito (su due profili in contemporanea). Allenatevi, insomma, a capire cosa appartiene a cosa. Ne avrete bisogno tra un paio di capitoli!  
  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)

## Tecnicamente

[DA AGGIUNGERE: come non dover rifare tutto da zero quando si cambia computer]  
  
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)
