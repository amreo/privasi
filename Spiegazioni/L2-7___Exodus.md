# Esodo

> Un ricordo personale dovrebbe essere un ricordo per se stessi da condividere al massimo con le persone care. Eppure, tramite i backup, foto e video passano per terze parti come Google Foto, rendendo quel ricordo condiviso con l'azienda in questione. Allo stesso modo, contenuti come documenti o fogli di calcolo (magari contenenti informazioni sensibili) passano per Google Drive. In questo capitolo, ci riapproprieremo dei ricordi e della discrezione

## In parole semplici

Come nel primo livello, se siete arrivati qui seguendo ogni capitolo, vogliamo farvi rendere conto dei passi fatti:
* Avete iniziato a usare app che vi rispettano e vedere quali non lo fanno
* Avete reso la vostra navigazione su internet non alla mercé di chi vuole capitalizzare sui comportamenti altrui
* Avete imparato a dare in pasto email farlocche nelle iscrizioni dubbie
* Avete detto no al tramutare la visione di un video in un'ulteriore invasione di privacy
* Avete smesso di farvi pedinare da Google Maps
* Avete aumentato la riservatezza sui social

Complimenti, siete ufficialmente nell'1% delle persone con i più alti livelli di privacy (o poco ci manca). D A M N :sunglasses: 

<div align="center"><img src="resources/L2-7_pic0.png"></div>  

Tuttavia, ci sono ancora un paio di cose che non abbiamo mai smesso di condividere con certe aziende, che permettono loro di avere scorci sulla nostra vita privata. Se si ha un telefono Android, per esempio, c'è una buona possibilità che si abbia il backup automatico su Google Foto di ciò che si scatta con la telecamera e delle immagini scaricate dalle varie app. A prescindere dal cellulare, invece, è anche molto probabile avere un Google Drive dove tenere file dai tipi più disparati (documenti, fogli di calcolo ecc.).  

Partendo dalle foto, non c'è garanzia riguardo a cosa Google ci faccia esattamente. Quello che sappiamo però è che in grado di distinguere in automatico le cose ritratte nelle foto, ovvero esiste un'intelligenza artificiale (AI) che analizza le immagini e le categorizza. Intelligenze simili pesano fior di giga, troppo spazio per poterle tenere sul telefono. Di conseguenza, devono trovarsi su un server esterno, il quale riceve le immagini in chiaro per poi elaborarle. Seppur non si sappia cosa se ne faccia, quindi, sappiamo di per certo che ne ha accesso.  

Per quanto riguarda Google Drive, un membro della comunità di supporto Google ha dichiarato che l'unico sistema di protezione è quello dato dalla password¹. Cercando infatti nella documentazione di Google non c'è nessun accenno a crittografia end to end (quella che garantisce a un utente che neanche l'azienda possa accedere al contenuto), ovvero Google *può* vedere i file.  

Il messaggio è semplice: se si vuole smettere di condividere con queste aziende la propria vita, bisogna iniziare a realizzare che le cose care vanno tenute vicino e quelle sensibili sotto chiave, soprattutto nel digitale. Questo capitolo può essere pesante da digerire, e non per niente lo si è tenuto come "gran finale"; digeritelo quindi alla velocità che preferite, ponderate le scelte e le situazioni. Come abbiamo fatto per le mail, il primo passo sarà quello di cancellare tutti i contenuti non rilevanti, siano essi foto, video o documenti. Fatto ciò porteremo sulla nostra macchina le foto e i video (sempre che ne abbiate), seguiti dai documenti NON condivisi con altre persone. Infine, illustreremo varie soluzioni per come gestire i file che si sono decisi di tenere, dove spetterà ovviamente a voi scegliere quella che più si addice alle vostre esigenze.

Benvenuti nell'esodo; sarà un lungo viaggio


## Cosa fare

#### Fase 1: cancellazione e interruzione

Iniziamo a eliminare quello che non ci serve più, e probabilmente a venire inondati da un mare di ricordi. Tratteremo questa guida da PC in quanto più scorrevole, ma potete farlo anche dalle singole app.  
Per foto e video andiamo su https://photos.google.com/  e su https://photos.google.com/archive
Per i file vari su https://drive.google.com/  

È possibile selezionare più file alla volta, come illustrato nelle GIF sotto, per cancellare più rapidamente.  

Bisogna anche evitare che i nuovi scatti nel frattempo finiscano di nuovo sui server Google. Per farlo, andiamo sul telefono (a meno che non sia un iPhone senza Google Foto installato) e disabilitiamo il backup di ogni cartella del dispositivo. Da ora in poi, le foto e i video non lasceranno più il telefono, quindi se avete qualcosa di davvero importante prima di arrivare alla prossima fase e avete paura che il telefono stia per lasciarvi, passatelo sul PC tramite cavetto.  

#### Fase 2: download e riordino

Dai link illustrati sopra scarichiamo tutto ciò che abbiamo tenuto. Su Google Foto potete selezionare più elementi in una volta premendo sulla spunta semi-trasparente sulla prima immagine e poi, tenendo premuto sulla tastiera il tasto SHIFT (la freccetta sopra a Ctrl) cliccando sull'ultima immagine. Attenzione ad avere abbastanza spazio sul computer.  

<div align="center"><img src="resources/L2-7_gif0.gif"></div>  

Su Google Drive basta invece selezionare tenendo premuto il tasto sinistro del mouse e fare poi tasto destro per far apparire il menù.  

<div align="center"><img src="resources/L2-7_gif1.gif"></div>  

Una volta scaricati, vi consigliamo anche di riordinare i file, magari in sottocartelle varie come "Foto 2016", "Foto 2017", "Documenti lavoro" ecc. per ritrovarle più facilmente in futuro.  

#### Fase 3: dove metterle
Le opzioni sono svariate, e inizieremo con quelle più semplici. C'è però prima da fare una distinzione fondamentale: vanno separati i ricordi dal resto, come tendiamo a fare nel mondo fisico.  

##### Ricordi: ridondanza
Taleb dice che la natura ama la ridondanza: ci facciamo male a un braccio, abbiamo l'altro. Un rene fa fatica, abbiamo l'altro. Ecco, diciamo che il computer è il braccio e che è meglio averne due. No, non due computer, due braccia. In altre parole, è meglio avere una copia dei propri ricordi salvata da qualche altra parte, nel caso il computer ci dovesse lasciare. E per farlo basta un **hard disk esterno** :D  

Per chi non lo sapesse, un hard disk esterno è come una super chiavetta USB, nel senso che ha molto più spazio, e assomiglia a uno scatolotto. Se non ne avete uno in casa, potete trovarli in qualsiasi negozio di elettronica a circa 50€, per 1 Tera di spazio (ovvero 1024 GB: *tanti* se si considera che lo spazio offerto "gratis" da Google sono 15GB tra Drive, mail e foto)². Lo attaccate al computer, passate i file che volete e il gioco è fatto.  

Per quanto riguarda il cellulare, vi consigliamo di passare ogni tot. le foto dal telefono al computer usando il cavetto USB (solitamente è lo stesso del caricatore). A prima vista può sembrare scomodo, ma questo vi permette anche di capire veramente quali cose volete davvero tenere e quali no. Quando ci basiamo sul cloud, infatti, viviamo con l'idea che tanto farà tutto per noi, che tra 5 o 10 anni potremo tornare a immergerci nuovamente nei ricordi con un semplice click, delegando alla macchina la nostra memoria. Ma la memoria — quella intima — è soprattutto nostra, perché non parliamo della lista della spesa ma di cose che hanno lasciato un segno dentro la nostra persona. E se vogliamo che queste cose rimangano davvero impresse, abbiamo bisogno di esercitare la memoria a ricordarle e il cervello a sforzarsi per selezionare quali sono importanti: non c'è scusa che tenga e potete chiedere conferma al vostro neurologo/neuropsichiatra di fiducia :D  
Tutto questo lo possiamo fare una volta ogni tanto (tipo una volta ogni mese/due o quando temiamo che il telefono ci stia per lasciare), con un semplice telefono, un cavetto e un computer. Vedetelo come un momento per voi stessi, perché alla fine quello è. E non si sa mai che deciderete di stampare qualche foto per voi particolarmente significativa.

##### Progetti, Lavoro ecc.: drive alternativi
Per quanto riguarda gli altri file invece, esistono almeno due alternative molto valide a Google Drive, e sono entrambe crittografate (ovvero l'azienda non ha accesso ai vostri file): Cryptee e CryptPad.  

[Cryptee](https://crypt.ee) ha un design pulito e intuitivo ed offre nella versione gratuita 100MB di spazio. Per 3€ al mese invece — perché non guadagnano monetizzando la vostra privacy, e i server non li regala nessuno a questo mondo — si ha accesso a 10GB di spazio. Dei due è quello che offre una maggiore versabilità tra i sistemi operativi PC e mobile ma una minor scelta nel tipo di file (documenti o foto). Tuttavia la differenza sostanziale è un'altra: Cryptee nasce come spazio *personale*, tipo cassaforte di file, tanto che oltre che la password vi chiede un'altra password di crittografia all'iscrizione, da immettere ogni volta che si prova ad accedere ai documenti; una doppia protezione insomma. Non è quindi possibile usarlo per condividere i file in giro con un link, bensì vanno prima esportati. Se cercate un mezzo di collaborazione invece, continuate a leggere.  

[CryptPad](https://cryptpad.fr/index.html) al contrario offre una gamma di file più ampia (documenti, tabelle, presentazioni ecc.) ed è pensato per essere condiviso con altre persone, che siano registrate o meno. Si può infatti collaborare in tempo reale sullo stesso file e, nei suoi piani a pagamento, è presente anche il pacchetto "Team" che per 15€ al mese che offre 50GB di spazio e 5 account premium da regalare al proprio team. Di default invece offre 50MB di spazio. Unica pecca risiede nella versatilità, nel senso che da cellulare non fila ancora liscio come l'olio (l'autore ha comunque dichiarato che si sarebbe concentrato su migliorare questo aspetto nel 2020³)

| Sito     	| Spazio gratuito 	| Spazio a pagamento 	| Client Desktop 	| App 	| Registrazione    	| Collaborazione in tempo reale 	| Varietà file 	| Ottimo per                	|
|----------	|-----------------	|--------------------	|----------------	|-----	|------------------	|-------------------------------	|--------------	|---------------------------	|
| Cryptee  	| 100MB           	| fino a 2000GB      	| ✔              	| ✔   	| obbligatoria     	| ✘                             	| bassa        	| cassaforte portatile      	|
| CryptPad 	| 50MB            	| fino a 50GB        	| ✘              	| ✘   	| non obbligatoria 	| ✔                             	| alta         	| collaborare e condividere 	|

##### Livello esperto: selfhosting
Se invece sapete dove mettere le mani, potete hostare [Nextcloud](https://nextcloud.com/) sul vostro server casalingo per gli scopi più disparati. Al contrario, se non sapete di cosa stiamo parlando, rifatevi ai punti precedenti.  
<br>  

Si potrebbe riassumere parte di questo capitolo in "abbiate tempo per voi stessi". Facciamo molte cose per comodità ed è difficile che ci fermiamo a riflettere su quali impatti negativi possano avere su di noi. Certo, questo non è un percorso sulla *fear of missing out*, la paura di perdersi qualcosa e rimanere indietro; ma il rallentare, il prendersi del tempo quando si ripensa alla propria vita, è fondamentale per essere più vicini a se stessi.  
Per quanto riguarda il resto invece, comprendiamo che per esigenze lavorative non tutti possano distaccarsi al 100% da Google Drive, soprattutto se siete dei sottoposti con pareri che non importano a chi ha potere decisionale (tentare non nuoce, ma ovviamente regolatevi voi). L'importante, comunque, è che alla fine le *vostre cose* — ovvero le uniche sulle quali potete davvero avere il controllo — non sono più alla mercé di certe mentalità che capitalizzerebbero pure la propria madre o l'aria che respirano se ne avessero la possibilità. Perché, come crediamo di aver dimostrato più e più volte nel corso di questo percorso, questa gente non ha freni e non basterebbero ventimila multe — che gli fanno il solletico — a impedirgli di fare quello che fanno, soprattutto se si considera che alcune di queste aziende sono in stretto contatto con organi politici e federali, in quello che Shoshana Zuboff ha definito "il fossato attorno al castello", ovvero un pararsi la schiena a vicenda.⁴ Ma bando alle ciance, perché da oggi vi possono toccare un po' meno. E soprattutto, vi siete appena riappropriati della vostra intimità :)
<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)   

## Appendice
¹ [Is is possible to encrypt a Google Drive folder or any document?](https://support.google.com/drive/thread/2560901)  
² Evitate gli SSD in quanto costano di più, hanno una capienza minore e sono assolutamente NON necessari per tenerci delle foto. Non si sa mai che il venditore ci provi :D  
³ https://blog.cryptpad.fr/2019/12/31/Looking-back-looking-forward/  
⁴ l'omonimo capitolo ne *Il Capitalismo della Sorveglianza* (S. Zuboff, *Il Capitalismo della Sorveglianza*, Roma, 2019, LUISS)