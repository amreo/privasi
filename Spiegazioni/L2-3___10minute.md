# Mail temporanee  

> Volete iscrivervi a un sito senza che questo vi riempia poi la casella di posta? Detto fatto  

## In parole semplici

Dall'ultimo capitolo del primo livello dovreste aver capito una cosa: iscriversi a un sito è facilissimo, cancellarsi può diventare un inferno. Quindi esattamente perché rischiare di farsi un account su un qualsiasi sito quando possiamo prima andare in avanscoperta, senza il rischio di trovarci bloccati nell'ennesima corrispondenza mail per rimuoverlo?  

Il sito 10 Minute Mail nasce proprio per questo: creare una casella di posta temporanea dalla durata di 10 minuti. Ci viene infatti fornito un indirizzo mail da usare come vogliamo, e uno spazio per ricevere mail. Ovvero il materiale necessario per iscriversi a un sito.  

Ovviamente se volessimo poi continuare a usare l'account sul sito in questione, è consigliato (se ne siete in grado) di andare nel profilo e modificare la mail con quella vostra vera e propria, spazzatura o meno, o in caso contrario di farvi semplicemente un nuovo account (con la mail vera e propria, spazzatura o meno). Inoltre, alcuni siti si sono ingegnati e riconoscono quando le mail inserite provengono da siti come 10minutemail, anche se parliamo di una fetta davvero piccola: in quel caso sta a voi decidere se non registrarvi proprio o se farlo con una mail vera e propria.  

Ma bando alle ciance!
## Cosa fare

Esistono più versioni, noi useremo https://10minutemail.net/ per un semplice motivo: al contrario di quella più famosa, funziona anche senza javascript, ovvero senza toccare uBlock Origin. L'unica funzione non visibile di base è il countdown dei 10 minuti che vi avvisa quanto tempo manca allo scadere della mail. Per risolvere potete:

1. o ricordarvi l'ora di quando avete aperto il sito
2. o impostare uBlock Origin come segue (javascript abilitato e jquery.com anche)

<div align="center"><img src="resources/L2-3_pic0.png"></div>  

---

Al centro trovate l'indirizzo di posta, mentre sotto 3 opzioni molto comode:
- **Ricarica pagina**: usatela per vedere se vi è arrivata una nuova mail
- **Dammi altri 10 minuti**: allunga la durata, fino a un massimo di 100 minuti
- **Dammi un altro indirizzo e-mail**: annulla l'attuale e ne genera uno nuovo

Infine, ancora più sotto trovate la casella di posta dove riceverete le mail.

<br>
<br>
<br>
[Torna al percorso](https://gitlab.com/etica-digitale/privasi/blob/master/Percorso.md)  
